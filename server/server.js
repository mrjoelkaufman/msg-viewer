'use strict';

var
    config = require('./config'),
    express = require('express'),
    cors = require('cors'),
    msg = require('./msg'),
    fs = require('fs'),
    app = express();


app.use(cors());



// get route
app.get('/email/:id',function(req, res)
{
    require = require;
    res.setHeader("Content-Type", "application/json");

    var id = req.params.id;
    var dir = config.msg.cachePath + id;
    

    if(fs.existsSync(dir + '/message.json')) {
        send();
    }else {

        msg.convert(id).then(function(){
           send();
        },function(){//error
           res.status(404).send('Sorry, we cannot find that!');
        });
    }

    function send(){
        res.sendFile(config.msg.cachePath + req.params.id +'/message.json');
    }

});


app.listen(config.serverPort, function(){
    console.log('listening on port' + config.serverPort + '.................');
});