var
    child_process  = require('child_process'),
    http           = require('http'),
    Q              = require('q'),
    config         = require('./config'),
    fs             = require('fs');


function convert(id){
    var deffred = Q.defer();

   // start spawn process

    if(! fs.existsSync(config.msg.tmpMsgDir + id)) {
        deffred.reject('!')
    }

    var proc = child_process.spawn(process.cwd() +'/libs/msg-extractor/ExtractMsg.py', ['--json', config.msg.tmpMsgDir + id ], {stdio: 'inherit', cwd: config.msg.cachePath});
   
      proc.on('exit',function(vent){
         deffred.resolve();
      });
   
      proc.on('error',function(err){
          console.log(err)
          deffred.reject(err);
      });

     
   

    return deffred.promise;
}


function getMsgFile(id)
{
    var promise = Q.defer();
    var file = fs.createWriteStream(config.msg.tmpMsgDir + id);// start a stream
    var request = http.get(config.msg.BaseUrl + id , function(response) {// download file

        response.pipe(file);// add data
        file.on('finish', function() {// close file
            file.close(cb);
            promise.resolve(id);// resolve promise
        });
    });

    return promise;
}



module.exports = {
    convert: convert
};

